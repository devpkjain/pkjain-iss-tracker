package com.pkjain.isstracker.home;

import com.pkjain.isstracker.models.PositionInfoWrapper;
import com.pkjain.isstracker.networking.NetworkError;
import com.pkjain.isstracker.networking.Service;

import java.util.Map;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by pkjain on 6/25/16.
 */
public class HomePresenter {
    private final Service service;
    private final HomeView view;
    private CompositeSubscription subscriptions;

    public HomePresenter(Service service, HomeView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();
    }

    public void getPositions(double lat, double lon, Map<String, Integer> options) {
        view.showWait();

        Subscription subscription = service.getPositions(lat, lon, options, new Service.GetPositionListCallback() {
            @Override
            public void onSuccess(PositionInfoWrapper cityListResponse) {
                view.removeWait();
                view.getPositionListSuccess(cityListResponse);
            }

            @Override
            public void onError(NetworkError networkError) {
                view.removeWait();
                view.onFailure(networkError.getAppErrorMessage());
            }

        });

        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
