package com.pkjain.isstracker.home;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Provides default location Listener
 */

public class DefaultLocationListener implements LocationListener {
    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
