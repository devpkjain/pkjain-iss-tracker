package com.pkjain.isstracker.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pkjain.isstracker.R;
import com.pkjain.isstracker.utils.PositionUtil;
import com.pkjain.isstracker.models.RiseInfo;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<RiseInfo> data;
    private Context context;

    public HomeAdapter(Context context, List<RiseInfo> data, OnItemClickListener listener) {
        this.data = data;
        this.listener = listener;
        this.context = context;
    }

    public List<RiseInfo> getData() {
        return data;
    }

    @Override
    public HomeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(HomeAdapter.ViewHolder holder, int position) {
        holder.click(data.get(position), listener);
        holder.date.setText(PositionUtil.getFormattedDate(data.get(position).risetime * 1000));
        holder.seconds.setText("Duration: " + Long.toString(data.get(position).duration) + " seconds");

        String images = "https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/pia21353-1041.jpg";

        Glide.with(context)
                .load(images)
                .into(holder.background);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onClick(RiseInfo Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView date, seconds;
        ImageView background;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            seconds = itemView.findViewById(R.id.duration);
            background = itemView.findViewById(R.id.image);

        }


        public void click(final RiseInfo cityListData, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(cityListData);
                }
            });
        }
    }
}
