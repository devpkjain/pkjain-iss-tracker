package com.pkjain.isstracker.home;

import com.pkjain.isstracker.models.PositionInfoWrapper;

/**
 * Created by pkjain on 6/25/16.
 */
public interface HomeView {

    void showWait();

    void removeWait();

    void removeAll();

    void onFailure(String appErrorMessage);

    void getPositionListSuccess(PositionInfoWrapper cityListResponse);
}
