package com.pkjain.isstracker.home;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pkjain.isstracker.BaseApp;
import com.pkjain.isstracker.R;
import com.pkjain.isstracker.models.PositionInfoWrapper;
import com.pkjain.isstracker.models.RiseInfo;
import com.pkjain.isstracker.networking.Service;
import com.pkjain.isstracker.utils.PositionUtil;

import javax.inject.Inject;

public class HomeActivity extends BaseApp implements HomeView {
    private static final Integer POSITION_COUNT = 5;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;
    private static final String ACCESS_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    @Inject
    public Service service;
    ProgressBar progressBar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView list;
    private HomePresenter presenter;
    private DefaultLocationListener locationListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().inject(this);

        renderView();
        init();

        presenter = new HomePresenter(service, this);
        requestLocationUpdate();
    }

    public void renderView() {
        setContentView(R.layout.activity_home);
        list = findViewById(R.id.list);
        refreshLayout = findViewById(R.id.refreshLayout);
        progressBar = findViewById(R.id.progress);

        setupRefreshAndLOadMoreListeners();
    }

    private void setupRefreshAndLOadMoreListeners() {
        locationListener = new DefaultLocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                requestIssPositions(location);
            }
        };

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                removeAll();
                requestLocationUpdate();
            }
        });
        list.addOnScrollListener(new EndlessOnScrollListener() {
            @Override
            public void onLoadMore() {
                onLoadMorePositions();
            }
        });
    }

    private void onLoadMorePositions() {
        //Todo: To be implemented later
    }

    public void removeAll() {
        ((HomeAdapter) list.getAdapter()).getData().clear();
        list.getAdapter().notifyDataSetChanged();
    }

    public void init() {
        list.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        Toast.makeText(getApplicationContext(), "Failed to retrive positions:" + appErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void getPositionListSuccess(PositionInfoWrapper positionInfoWrapper) {

        HomeAdapter adapter = new HomeAdapter(getApplicationContext(), positionInfoWrapper.response,
                new HomeAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(RiseInfo Item) {
                        Toast.makeText(getApplicationContext(), Item.duration + "",
                                Toast.LENGTH_LONG).show();
                    }
                });

        list.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionGranted();
                } else {
                    finish();
                }
                return;
            }
        }
    }

    private void requestLocationUpdate() {
        Location lastKnownLocation = PositionUtil.getLastKnownLocation(this);
        if (lastKnownLocation != null) {
            requestIssPositions(lastKnownLocation);
        } else {
            if (hasGpsProvider()) {
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    LocationManager systemService = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);//.getAllProviders().contains(LocationManager.GPS_PROVIDER);
                    systemService.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 10, 1, locationListener);
                    showWait();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            } else {
                Toast.makeText(getApplicationContext(), "Failed to retrive location:", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onPermissionGranted() {
        requestLocationUpdate();
    }

    private boolean hasGpsProvider() {
        return ((LocationManager) this.getSystemService(Context.LOCATION_SERVICE)).getAllProviders().contains(LocationManager.GPS_PROVIDER);
    }

    /**
     * Request ISS passing times for requested location
     *
     * @param location location of requested position
     */
    private void requestIssPositions(@NonNull Location location) {
        requestIssPositions(location.getLatitude(), location.getLongitude());
    }

    /**
     * Request ISS passing times for requested lattitude & Longitude
     *
     * @param lat lattitude
     * @param lon longitude
     */
    private void requestIssPositions(double lat, double lon) {
        presenter.getPositions(lat, lon, PositionUtil.getMap(10, POSITION_COUNT));
    }
}