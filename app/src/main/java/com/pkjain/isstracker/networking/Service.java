package com.pkjain.isstracker.networking;


import com.pkjain.isstracker.models.PositionInfoWrapper;

import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.pkjain.isstracker.utils.PositionUtil.getFormattedLocationInDegree;

/**
 * Created by pkjain on 6/25/16.
 */
public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getPositions(double lat, double lon, Map<String, Integer> options, final GetPositionListCallback callback) {

        return networkService.getPositions(getFormattedLocationInDegree(lat), getFormattedLocationInDegree(lon), options)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PositionInfoWrapper>>() {
                    @Override
                    public Observable<? extends PositionInfoWrapper> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PositionInfoWrapper>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(PositionInfoWrapper cityListResponse) {
                        callback.onSuccess(cityListResponse);

                    }
                });
    }

    public interface GetPositionListCallback {
        void onSuccess(PositionInfoWrapper positionInfoWrapper);

        void onError(NetworkError networkError);
    }
}
