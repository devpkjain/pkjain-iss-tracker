package com.pkjain.isstracker.networking;


import com.pkjain.isstracker.models.PositionInfoWrapper;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by pkjain on 6/25/16.
 */
public interface NetworkService {
    /**
     * Provides List of riseing times for international space station
     * @param lat lattitude
     * @param lon longitude
     * @param options optional parameters
     * @return positionInfoWrapper
     */
    @GET("/iss-pass.json?")
    Observable<PositionInfoWrapper>  getPositions(
            @Query("lat") String lat,
            @Query("lon") String lon,
            @QueryMap(encoded=true) Map<String, Integer> options);
}
