package com.pkjain.isstracker.deps;


import com.pkjain.isstracker.home.HomeActivity;
import com.pkjain.isstracker.networking.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pkjain on 6/28/16.
 */
@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(HomeActivity homeActivity);
}
