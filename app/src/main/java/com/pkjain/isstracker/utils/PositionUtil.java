package com.pkjain.isstracker.utils;

import android.content.Context;
import android.icu.text.TimeZoneNames;
import android.location.Location;
import android.location.LocationManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


/**
 *
 */

public abstract class PositionUtil {

    public static Map getMap(Integer alt, Integer n){
        Map<String, Integer> data = new HashMap<>();
        data.put("alt", alt);
        data.put("n", n);
        return data;
    }

    public static String getFormattedLocationInDegree(double location) {
        return Location.convert(location, Location.FORMAT_DEGREES);
    }

    public static Location getLastKnownLocation(Context context){
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);//.getAllProviders().contains(LocationManager.GPS_PROVIDER);

            return  locationManager != null ? locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER) : null;
        } catch (SecurityException e) {
//            dialogGPS(this.getContext()); // lets the user know there is a problem with the gps
        }
        return null;
    }

    public static String getFormattedDate(long millis){
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        format.setTimeZone(TimeZone.getDefault());
        return format.format(new Date(millis));
    }
}
