package com.pkjain.isstracker.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This model provides rising date and duration information for international space station
 */

public class RiseInfo {

    public RiseInfo(long risetime, long duration) {
        this.risetime = risetime;
        this.duration = duration;
    }

    @SerializedName("risetime")
    @Expose
    public final long risetime;

    @SerializedName("duration")
    @Expose
    public final long duration;
}
