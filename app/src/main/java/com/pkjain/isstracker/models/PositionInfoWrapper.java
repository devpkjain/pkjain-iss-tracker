package com.pkjain.isstracker.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;
/**
 * This model provides response information for international space station passing times
 */
@Generated("org.jsonschema2pojo")
public class PositionInfoWrapper {

    @SerializedName("message")
    @Expose
    public final String message;

    @SerializedName("request")
    @Expose
    public final PositionInfo request;

    @SerializedName("response")
    @Expose
    public final List<RiseInfo> response = new ArrayList<RiseInfo>();

    public PositionInfoWrapper(String message, PositionInfo request) {
        this.message = message;
        this.request = request;
    }
}