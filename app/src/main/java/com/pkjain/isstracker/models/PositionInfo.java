package com.pkjain.isstracker.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
/**
 * This model provides request parameters for a particular coordinate on earth
 */
@Generated("org.jsonschema2pojo")
public class PositionInfo {

    public PositionInfo(int altitude, long datetime, float latitude, float longitude, int passes) {
        this.altitude = altitude;
        this.datetime = datetime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.passes = passes;
    }

    @SerializedName("altitude")
    @Expose
    public final int altitude;

    @SerializedName("datetime")
    @Expose
    public final  long datetime;

    @SerializedName("latitude")
    @Expose
    public final  float latitude;

    @SerializedName("longitude")
    @Expose
    public final  float longitude;

    @SerializedName("passes")
    @Expose
    public final  int passes;
}