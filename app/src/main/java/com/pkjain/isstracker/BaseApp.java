package com.pkjain.isstracker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pkjain.isstracker.deps.DaggerDeps;
import com.pkjain.isstracker.deps.Deps;
import com.pkjain.isstracker.networking.NetworkModule;

import java.io.File;

/**
 * Created by pkjain on 6/28/16.
 */
public class BaseApp  extends AppCompatActivity{
    Deps deps;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();

    }

    public Deps getDeps() {
        return deps;
    }
}
