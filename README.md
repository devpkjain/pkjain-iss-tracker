# ISS Tracker PKJain

Uses MVP, Dagger 2, RxJava and Retrofit 2

This project uses Dagger 2 for dependency injection and RxJava & Retrofit for Client Server Communication

Major commponents
1. Retrieve Location coordinates using Location Manager
2. Retrieve International space station passing times.
3. Populate duration and date on a recycler view
4. Implement Pull to refresh functionality to refresh the details.
5. Implement Load More Functionality to load more passing times.
6. Add unit test to validate functionality

